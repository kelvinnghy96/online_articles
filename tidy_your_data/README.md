# Visualising proportions
Are you still not using a ‘tidy’ dataset? Here’s why you should

## Data
- srcdata/demo_messy.csv messy dataset (hospital beds per capita, from the World Bank)
- srcdata/demo_tidy.csv tidied version of the dataset 

### Article
https://towardsdatascience.com/are-you-still-not-using-a-tidy-dataset-here-s-why-you-should-5c4b79d5c9bc